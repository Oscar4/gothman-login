import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect }  from 'react';
import { StyleSheet, Text, View, Image, Button, Alert,
  TouchableOpacity, Animated, Easing, TextInput
 } from 'react-native';



export default function App() {
  animatedLogoScale = new Animated.Value(30)
  animatedWelcomeOppacity = new Animated.Value(0)
  animatedLogoTop = new Animated.Value(50)
  animatedBatmanIn = new Animated.Value(5)
  animatedButtonsIn = new Animated.Value(250)
  animateLogoOut = new Animated.Value(0)
  animateGetAccessIn = new Animated.Value(500)
  const [isSignUp, setIsSignUp] = useState(false)

  useEffect(() => {
    !isSignUp ? animate() : animete2()
    
  }, [isSignUp])
// https://dribbble.com/shots/13905960-Signup-page-Concept-CreateWithAdobeXD
  const animate = () => {
    Animated.sequence([
      Animated.timing(animatedLogoScale,
        {
          toValue: 1,
          duration: 900,
          useNativeDriver: false,
        }),
      Animated.timing(animatedWelcomeOppacity,
        {
          toValue: 0.80,
          duration: 250,
          useNativeDriver: false
        }),
      Animated.timing(animatedLogoTop,
        {
          toValue: 0,
          duration: 350,
          useNativeDriver: false
        }),
      Animated.parallel([
        Animated.timing(animatedBatmanIn,
          {
            toValue: 1,
            duration: 900,
            useNativeDriver: false,
          }),
          Animated.timing(animatedButtonsIn,{
            toValue: 0,
            duration: 900,
            useNativeDriver: false,
          })
          
      ])
        
    ]).start()
  }

  const animete2 = () => {
    Animated.sequence([
      Animated.timing(animateLogoOut, {
        toValue: 500,
        duration: 900,
        useNativeDriver: false
      }),
      Animated.timing(animateGetAccessIn, {
        toValue: 0,
        duration: 500,
        useNativeDriver: false
      })
    ]).start()
  }

  const goSignUp = () => {
    console.log("signup")
    setIsSignUp(true)
  }

  function AppHome(){
    return(
      <View style={styles.container}>
      <Image 
        style={{
          width: '100%',
          height: '100%',
          position: 'absolute',
          //zIndex: -1
        }}
        source={require('./src/assets/batman_background.png')}
      />
      <Animated.Image 
        style={{
          width: 400,
          height: 800,
          position: 'absolute',
          transform:[{
            scale: 1//animatedBatmanIn
          }],
          //zIndex: -1
        }}
        source={require('./src/assets/batman_alone.png')}
      />
      {!isSignUp ? 
        <Animated.View style={{
          alignItems: 'center',
          justifyContent: 'center',
          textAlign: 'center',
         
          
        }}>
          <Animated.Image
            style={{
              marginTop: 250,
              height: 100,
              width: 200,
              top: animatedLogoTop,
              transform: [{
                scale: animatedLogoScale
              }]
            }}
            source={require('./src/assets/batman_logo.png')}
          />
          <Animated.View style={{
            opacity: animatedWelcomeOppacity,
            transform: [{
              translateY: animateLogoOut
            }]
          }}>
            <Text style={{
              textAlign: 'center',
              color: '#fff',
              fontSize: 22
            }}>
              WELCOME TO
            </Text>
            <Text style={{
              color: '#fff',
              fontSize: 35,
              marginBottom: 20
            }}>
              GOTHAM CITY
            </Text>
          </Animated.View>
          
          <Animated.View style={{
            opacity: 0.8,
            transform:[{
              translateY: animatedButtonsIn
            }]
            
          }}>
          <TouchableOpacity
            style={styles.batiButton}
            onPress={() => Alert.alert('login')}
          >
            <View style={{flexDirection: 'row'
              }}>
                <Text style={{
                  paddingTop: 5,
                  position: 'absolute',
                  fontSize: 18
                }}>LOGIN</Text>
                <Image
                  style={{
                    height: 25,
                    width: 60,
                    //position: 'absolute',
                    left: 120,
                    top: 8,
                    transform: [{
                      rotate: "-38deg"
                    }]
  
                  }}
                  source={require('./src/assets/batman_logo.png')}
                />
                
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.batiButton}
            onPress={goSignUp}
          >
              <View style={{flexDirection: 'row'
              }}>
                <Text style={{
                  paddingTop: 5,
                  position: 'absolute',
                  fontSize: 18
                }}>SIGNUP</Text>
                <Image
                  style={{
                    height: 35,
                    width: 70,
                    //position: 'absolute',
                    right: 120,
                    top: 10,
                    transform: [{
                      rotate: "30deg"
                    }]
                  }}
                  source={require('./src/assets/batman_logo.png')}
                />
                
            </View>
          </TouchableOpacity>
          </Animated.View>
        </Animated.View>
        :<Animated.View style={{
          transform:[{
            translateY: animateGetAccessIn
          }]
        }}>
          <Image style={{
            
            width: 400,
            height: 500,
            position: 'absolute',
            top: 200,
            right: -40,
            
          }}
            source={require('./src/assets/city.png')} />
        
        
        <View style={{
          marginTop: 390
        }}>
          
          <Text style={{
            textAlign: 'center',
            color: '#fff',
            fontSize: 35
          }}>GET ACCESS</Text>
          <TextInput style={styles.input}
            placeholder="Username"
             />
          <TextInput style={styles.input}
            placeholder="Email"
            />
          <TextInput style={styles.input}
            placeholder="Password"
             />
          <TouchableOpacity
            style={[styles.batiButton, {marginTop: 10}]}
            onPress={() => setIsSignUp(false)}
          >
              <View style={{flexDirection: 'row'
              }}>
                <Text style={{
                  paddingTop: 5,
                  position: 'absolute',
                  fontSize: 18
                }}>SIGNUP</Text>
                <Image
                  style={{
                    height: 35,
                    width: 70,
                    //position: 'absolute',
                    right: 120,
                    top: 10,
                    transform: [{
                      rotate: "30deg"
                    }]
                  }}
                  source={require('./src/assets/batman_logo.png')}
                />
                
            </View>
          </TouchableOpacity>
        </View>
        </Animated.View>
    }
      
     
        <StatusBar style="auto" />
      </View>
    
    )
  }
  {
      
    /* <AppHome/>
  <NavigationContainer>
    <Stack.Navigator initialRouteName="Signup">
      <Stack.Screen name="appHome" component={AppHome} />
      <Stack.Screen name="Signup" component={SignUp} />
    </Stack.Navigator>
  </NavigationContainer>
  */}
  return (
    <AppHome/>
  );
}

const styles = StyleSheet.create({
  /* colors
  negro #080A0C
  amarillo #F9D40D
  */
 
  container: {
    flex: 1,
    backgroundColor: '#080A0C',
    alignItems: 'center',
    justifyContent: 'center',
    
  },
  batiButton:{
    color: '#000',
    height: 50,
    width: 300,
    backgroundColor: "#F9D40D",
    marginBottom: 12,
    alignItems: 'center',
    justifyContent: 'center',
  },
  input:{
    height: 50,
    width: 300,
    borderColor: '#fff',
    color: '#fff',
    fontSize: 16,
    borderWidth: 1,
    marginTop: 5,
    padding: 10

  }
});
